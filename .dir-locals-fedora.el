;; Allow flycheck to use clang or gcc with C++11

((c-mode
  (flycheck-clang-language-standard . "gnu11")
  (flycheck-gcc-language-standard . "gnu11")
  (flycheck-gcc-args . ("-Wall"
                        "-Os"
                        "-DF_CPU=1000000UL"
                        "-Wno-ignored-attributes"
                        "-Wno-attributes"
                        "-D__AVR_ATtiny85__"))
  
  (flycheck-clang-args . ("-Wall"))

  (flycheck-c/c++-gcc-executable . "/usr/bin/avr-gcc")
  ;;(flycheck-disabled-checkers . (c/c++-clang irony))

  (flycheck-gcc-include-path . (
                                "/usr/avr/include"
                                "/usr/avr/include/avr"
                                "/usr/avr/include/sys"
                                "/usr/avr/include/util"
                                ))

))
