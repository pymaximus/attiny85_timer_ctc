avrtest
=======

Tests AVR GCC workflow on Fedora 20

Target - attiny85 on STK500 or standalone

Execute the following

    make clean
    make
    make dl

Now, PORTB0/OC0A of attiny85 on the STK500 will toggle periodically.

![Screenshot](images/DS2_2017427202820.png)