/******************************************************************************
 Title:    Flash LED on PORTB using Timer0 CTC and Toggle mode
 Author:   Frank Singleton
 Hardware: Attiny85 at 1Mhz, STK200 compatible starter kit

 Description:
 This example demonstrates TIMER0/CTC/Toggle mode by flashing a led on PORTB0
 periodically at approximately 12.207Hz (~81.92mSec)
 1000000/1024/40/2
 Based on 1Mhz internal clock (8Mhz and DIV/8 set in fuse).
*******************************************************************************/

#define F_CPU 1000000UL

#include <inttypes.h>
#include <stdbool.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

/********************************************************************************
 Interrupt Routines
********************************************************************************/

ISR(TIMER0_COMPA_vect) {
    // XOR PORTB with 0x01 to toggle the second bit up
    //PORTB = PORTB ^ 0x01;

    // NOTE: toggle mode is enabled so NO need to explicitly do anything to see
    // PORTB0 / OC0A toggle :-)
}
/********************************************************************************
 Main
********************************************************************************/
int main(void) {

    /* disable global interrupts */
    cli();

    /* Set pin PB0 as output */
    DDRB |= (1 << PB0);

    /* init timer0 */
    TCNT0 = 0;
    TCCR0A = 0;
    TCCR0B = 0;

    TCCR0A |= (1 << COM0A0); /* Timer0 in toggle mode Table 11-2 */
    TCCR0A |= (1 << WGM01); /* Start timer in CTC mode Table 11.5 */
    TCCR0B |= (1 << CS00) | (1 << CS02); /* CLK/1024 ,Prescaler table 11.6 */
    OCR0A = 39; /* CTC Compare value, 40 = 39 + 1 */

    /* enable global interrupts only if required */
    //sei();

    while (true) {
        ;
    }
}
